package shop.velox.availability.model;

public enum AvailabilityStatus {
  ALWAYS_AVAILABLE,
  NOT_AVAILABLE,
  IN_STOCK
}
