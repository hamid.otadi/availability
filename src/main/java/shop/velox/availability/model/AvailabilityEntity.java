package shop.velox.availability.model;

import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import shop.velox.commons.model.AbstractEntity;


@Entity
@Table(name = "AVAILABILITIES")
public class AvailabilityEntity extends AbstractEntity {

  @PrePersist
  private void prePersistFunction() {
    if (StringUtils.isEmpty(id)) {
      id = UUID.randomUUID().toString();
    }
  }

  @Column(unique = true, nullable = false)
  private String id;

  @Column(nullable = false)
  private String articleId;

  @Column(nullable = false)
  private BigDecimal quantity;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private AvailabilityStatus status;

  @Column(nullable = false)
  private int replenishmentTime;

  public AvailabilityEntity() {
    // For JPA
  }

  public AvailabilityEntity(String articleId, BigDecimal quantity, AvailabilityStatus status) {
    this.articleId = articleId;
    this.quantity = ObjectUtils.defaultIfNull(quantity, BigDecimal.ZERO);
    this.status = ObjectUtils.defaultIfNull(status, AvailabilityStatus.NOT_AVAILABLE);
  }

  public AvailabilityEntity(String articleId, BigDecimal quantity, AvailabilityStatus status,
      int replenishmentTime) {
    this.articleId = articleId;
    this.quantity = ObjectUtils.defaultIfNull(quantity, BigDecimal.ZERO);
    this.status = ObjectUtils.defaultIfNull(status, AvailabilityStatus.NOT_AVAILABLE);
    this.replenishmentTime = replenishmentTime;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }

  public String getId() {
    return id;
  }

  public String getArticleId() {
    return articleId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public AvailabilityStatus getStatus() {
    return status;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public void setStatus(AvailabilityStatus status) {
    this.status = status;
  }

  public int getReplenishmentTime() {
    return replenishmentTime;
  }

  public void setReplenishmentTime(int replenishmentTime) {
    this.replenishmentTime = replenishmentTime;
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
