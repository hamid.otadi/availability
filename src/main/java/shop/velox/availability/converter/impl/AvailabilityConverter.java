package shop.velox.availability.converter.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.availability.api.dto.AvailabilityDto;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.commons.converter.Converter;

@Component
public class AvailabilityConverter implements Converter<AvailabilityEntity, AvailabilityDto> {

  private static final Logger LOG = LoggerFactory.getLogger(AvailabilityConverter.class);
  private final MapperFacade mapperFacade;

  public AvailabilityConverter() {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    mapperFactory
        .classMap(AvailabilityEntity.class, AvailabilityDto.class)
        .byDefault()
        .register();

    mapperFacade = mapperFactory.getMapperFacade();
  }

  @Override
  public AvailabilityDto convertEntityToDto(AvailabilityEntity availabilityEntity) {
    AvailabilityDto availabilityDto = getMapperFacade()
        .map(availabilityEntity, AvailabilityDto.class);
    LOG.debug("Converted {} to {}", availabilityEntity, availabilityDto);
    return availabilityDto;
  }

  @Override
  public AvailabilityEntity convertDtoToEntity(AvailabilityDto availabilityDto) {
    AvailabilityEntity availabilityEntity = getMapperFacade()
        .map(availabilityDto, AvailabilityEntity.class);
    LOG.debug("Converted {} to {}", availabilityDto, availabilityEntity);
    return availabilityEntity;
  }

  public MapperFacade getMapperFacade() {
    return mapperFacade;
  }
}
