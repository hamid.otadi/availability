package shop.velox.availability.service.impl;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.availability.dao.AvailabilityRepository;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.availability.service.AvailabilityService;

@Service
public class AvailabilityServiceImpl implements AvailabilityService {

  private static final Logger LOG = LoggerFactory.getLogger(AvailabilityServiceImpl.class);

  private final AvailabilityRepository availabilityRepository;

  public AvailabilityServiceImpl(@Autowired AvailabilityRepository availabilityRepository) {
    this.availabilityRepository = availabilityRepository;
  }

  @Override
  public AvailabilityEntity createAvailability(final AvailabilityEntity availabilityEntity) {
    if (StringUtils.isBlank(availabilityEntity.getArticleId())) {
      LOG.info("cannot create availability for article with no articleId {}, it already exists",
          availabilityEntity);
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "articleId missing");
    }

    Optional<AvailabilityEntity> existingAvailability = availabilityRepository
        .findOneByArticleId(availabilityEntity.getArticleId());

    if (existingAvailability.isPresent()) {
      LOG.info(
          "cannot create availability for article with enclosed articleId {}, it already exists",
          availabilityEntity);
      throw new ResponseStatusException(HttpStatus.CONFLICT,
          "Availability for article with id: " + availabilityEntity.getArticleId() +
              " already exists");
    }

    AvailabilityEntity createdAvailabilityEntity = availabilityRepository
        .saveAndFlush(availabilityEntity);
    LOG.info("Created availability {}", availabilityEntity);
    return createdAvailabilityEntity;
  }

  @Override
  public Optional<AvailabilityEntity> findById(String id) {
    return availabilityRepository.findOneById(id);
  }

  @Override
  public Page<AvailabilityEntity> findAll(Pageable pageable) {
    return availabilityRepository.findAll(pageable);
  }

  @Override
  public Page<AvailabilityEntity> findAllByArticleIds(Pageable pageable,
      List<String> articleIds) {
    if (isEmpty(articleIds)) {
      return availabilityRepository.findAll(pageable);
    }
    return availabilityRepository.findAllByArticleIdIn(articleIds, pageable);
  }

  @Override
  public AvailabilityEntity updateAvailability(String id, AvailabilityEntity availabilityEntity) {

    LOG.info("Update {}, {}", id, availabilityEntity);

    if (StringUtils.isNotBlank(availabilityEntity.getId()) && !StringUtils
        .equals(id, availabilityEntity.getId())) {
      throw new ResponseStatusException(HttpStatus.CONFLICT,
          "Cannot manually change the availability id");
    }

    AvailabilityEntity existingAvailability = availabilityRepository.findOneById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    if (!StringUtils
        .equals(existingAvailability.getArticleId(), availabilityEntity.getArticleId())) {
      throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot update the articleId");
    }

    if (availabilityEntity.getQuantity() != null) {
      existingAvailability.setQuantity(availabilityEntity.getQuantity());
    }
    if (availabilityEntity.getStatus() != null) {
      existingAvailability.setStatus(availabilityEntity.getStatus());
    }

    if (availabilityEntity.getReplenishmentTime() != existingAvailability.getReplenishmentTime()) {
      existingAvailability.setReplenishmentTime(availabilityEntity.getReplenishmentTime());
    }

    AvailabilityEntity updatedAvailabilityEntity = availabilityRepository
        .saveAndFlush(existingAvailability);
    return updatedAvailabilityEntity;
  }

  @Override
  @Transactional
  public void deleteAvailability(final String id) {

    LOG.info("removeAvailability {}", id);

    if (StringUtils.isBlank(id)) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          "Cannot remove availability without id");
    }

    AvailabilityEntity entity = availabilityRepository.findOneById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    availabilityRepository.deleteAvailabilityEntityById(id);
  }
}
