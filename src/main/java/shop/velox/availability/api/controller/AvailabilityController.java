package shop.velox.availability.api.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.availability.api.dto.AvailabilityDto;

@Tag(name = "Availability", description = "the Availability API")
@RequestMapping("")
public interface AvailabilityController {

  String AVAILABILITIES_PATH = "/availabilities";

  String GET_AVAILABILITIES_ARTICLE_ID_FILTER_NAME = "articleId";

  @Operation(summary = "creates Availability", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "returns the created availability", content = @Content(schema = @Schema(implementation = AvailabilityDto.class))),
      @ApiResponse(responseCode = "409", description = "availability already exists", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "Mandatory parameters are missing. (e.g. Article Id)", content = @Content(schema = @Schema())),})
  @PostMapping(value = AVAILABILITIES_PATH, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
  ResponseEntity<AvailabilityDto> createAvailability(
      @Valid @RequestBody AvailabilityDto availabilityDto);


  @Operation(summary = "Get Availability by id", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = AvailabilityDto.class))),
      @ApiResponse(responseCode = "404", description = "availability not found"),
      @ApiResponse(responseCode = "422", description = "Mandatory parameters are missing. (e.g. Article Id)", content = @Content(schema = @Schema())),})
  @GetMapping(value = AVAILABILITIES_PATH + "/{id}")
  ResponseEntity<AvailabilityDto> getAvailability(
      @Parameter(description = "Id of the Availability. Cannot be empty.", required = true) @PathVariable("id") final String id);


  @Operation(summary = "Get Availabilities", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = AvailabilityDto.class))),})
  @GetMapping(value = AVAILABILITIES_PATH)
  ResponseEntity<Page<AvailabilityDto>> getAvailabilities(
      @Parameter(description = "Pagination will be ignored when articleIds are provided")
      @SortDefault.SortDefaults({@SortDefault(sort = GET_AVAILABILITIES_ARTICLE_ID_FILTER_NAME, direction = Sort.Direction.ASC)})
          final Pageable pageable,
      @Parameter(description = "Filter by Id of the Article. Return all availabilities matching of the given articleIds")
      @RequestParam(name = GET_AVAILABILITIES_ARTICLE_ID_FILTER_NAME, required = false)
      final List<String> articleIds);


  @Operation(summary = "Updates an Availability", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = AvailabilityDto.class))),
      @ApiResponse(responseCode = "404", description = "availability not found", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "409", description = "availability id cannot be changed", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "Mandatory data missing", content = @Content(schema = @Schema()))})
  @PatchMapping(value = AVAILABILITIES_PATH + "/{id}", consumes = APPLICATION_JSON_VALUE)
  ResponseEntity<AvailabilityDto> updateAvailability(
      @Parameter(description = "Id of the Availability. Cannot be empty.", required = true) @PathVariable("id") String id,
      @Parameter(description = "Availability to update. Cannot be empty.", required = true) @Valid @RequestBody AvailabilityDto availabilityDto);


  @Operation(summary = "Delete Availability by id", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "successful operation"),
      @ApiResponse(responseCode = "404", description = "availability not found")})
  @DeleteMapping(value = AVAILABILITIES_PATH + "/{id}")
  ResponseEntity<Void> deleteAvailability(
      @Parameter(description = "Id of the Availability. Cannot be empty.", required = true) @PathVariable("id") final String id);
}
