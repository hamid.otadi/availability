package shop.velox.availability.api.controller.impl;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.availability.api.controller.AvailabilityController;
import shop.velox.availability.api.dto.AvailabilityDto;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.availability.service.AvailabilityService;
import shop.velox.commons.converter.Converter;

@RestController
public class AvailabilityControllerImpl implements AvailabilityController {

  private static final Logger LOG = LoggerFactory.getLogger(AvailabilityControllerImpl.class);

  private final AvailabilityService availabilityService;

  private final Converter<AvailabilityEntity, AvailabilityDto> availabilityConverter;

  public AvailabilityControllerImpl(@Autowired AvailabilityService availabilityService,
      @Autowired Converter<AvailabilityEntity, AvailabilityDto> availabilityConverter) {
    this.availabilityService = availabilityService;
    this.availabilityConverter = availabilityConverter;
  }

  @Override
  public ResponseEntity<AvailabilityDto> createAvailability(final AvailabilityDto availabilityDto) {
    AvailabilityDto body = availabilityConverter
        .convertEntityToDto(availabilityService
            .createAvailability(availabilityConverter.convertDtoToEntity(availabilityDto)));
    return ResponseEntity.status(HttpStatus.CREATED).body(body);
  }

  @Override
  public ResponseEntity<AvailabilityDto> getAvailability(final String id) {
    AvailabilityEntity availabilityEntity = availabilityService.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    AvailabilityDto availabilityDto = availabilityConverter.convertEntityToDto(availabilityEntity);
    return ResponseEntity.status(HttpStatus.OK).body(availabilityDto);
  }

  @Override
  public ResponseEntity<Page<AvailabilityDto>> getAvailabilities(Pageable pageable,
      final List<String> articleIds) {

    LOG.info("getAvailabilities with articleIds: {}", String.join(", ", emptyIfNull(articleIds)));

    Page<AvailabilityEntity> entitiesPage;
    Pageable responsePageable;
    if (isEmpty(articleIds)) {
      responsePageable = pageable;
      entitiesPage = availabilityService.findAll(responsePageable);
    } else {
      responsePageable = Pageable.unpaged();
      entitiesPage = availabilityService.findAllByArticleIds(responsePageable, articleIds);
    }
    Page<AvailabilityDto> dtosPage = availabilityConverter.convert(responsePageable, entitiesPage);
    return ResponseEntity.ok(dtosPage);
  }

  @Override
  public ResponseEntity<AvailabilityDto> updateAvailability(final String id,
      final AvailabilityDto availabilityDto) {
    LOG.info("updateAvailability {}, {}", id, availabilityDto);
    AvailabilityEntity availabilityEntity = availabilityService
        .updateAvailability(id, availabilityConverter.convertDtoToEntity(
            availabilityDto));
    return ResponseEntity.ok(availabilityConverter.convertEntityToDto(availabilityEntity));
  }

  @Override
  public ResponseEntity<Void> deleteAvailability(final String id) {
    LOG.info("removePrice {}", id);
    availabilityService.deleteAvailability(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    return ex.getBindingResult()
        .getAllErrors()
        .stream()
        .collect(Collectors.toMap(
            error -> ((FieldError) error).getField(),
            error -> defaultIfEmpty(EMPTY, error.getDefaultMessage())));
  }
}
