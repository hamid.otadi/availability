package shop.velox.availability.api.dto;

import static java.util.Collections.emptyList;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import io.swagger.v3.oas.annotations.media.Schema;

public class AvailabilityListDto
{
    @Schema(description = "A list of Availabilities")
    private List<AvailabilityDto> availabilities;

    public AvailabilityListDto()
    {
        this(emptyList());
    }

    public AvailabilityListDto(final List<AvailabilityDto> availabilities)
    {
        this.availabilities = emptyIfNull(availabilities);
    }

    public List<AvailabilityDto> getAvailabilities()
    {
        return availabilities;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
