package shop.velox.availability.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shop.velox.availability.model.AvailabilityStatus;

public class AvailabilityDto {

  private static final Logger LOG = LoggerFactory.getLogger(AvailabilityDto.class);

  @Schema(description = "Unique identifier of the Availability.", example = "5dc618af-af49-4adc-bccd-4d17aeff7526")
  private String id;

  @Schema(description = "Unique identifier of the article.", example = "123", required = true)
  @Size(min = 3, max = 50)
  private String articleId;

  @Schema(description = "Non-negative available quantity of the article.", example = "10")
  @PositiveOrZero
  private BigDecimal quantity;

  @Schema(description = "Availability stock status of the article.", example = "IN_STOCK")
  @Valid
  private AvailabilityStatus status;

  @Schema(description = "Standard lead time in days for article quantity replenishment.", example = "14", defaultValue = "0")
  @PositiveOrZero
  private int replenishmentTime;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getArticleId() {
    return articleId;
  }

  public void setArticleId(String articleId) {
    this.articleId = articleId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public AvailabilityStatus getStatus() {
    return status;
  }

  public void setStatus(AvailabilityStatus status) {
    this.status = status;
  }

  public int getReplenishmentTime() { return replenishmentTime; }

  public void setReplenishmentTime(int replenishmentTime) { this.replenishmentTime = replenishmentTime; }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
