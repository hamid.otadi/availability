package shop.velox.availability.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.availability.model.AvailabilityStatus;

public interface AvailabilityRepository extends JpaRepository<AvailabilityEntity, String> {

  Optional<AvailabilityEntity> findOneById(String id);

  Optional<AvailabilityEntity> findOneByArticleId(String articleId);

  Optional<AvailabilityEntity> findOneByArticleIdAndStatus(String articleId,
      AvailabilityStatus status);

  Page<AvailabilityEntity> findAllByStatus(AvailabilityStatus status, Pageable pageable);

  Page<AvailabilityEntity> findAllByQuantityGreaterThanEqual(BigDecimal quantity,
      Pageable pageable);

  Page<AvailabilityEntity> findAllByQuantityGreaterThanEqualAndStatus(BigDecimal quantity,
      AvailabilityStatus status, Pageable pageable);

  Page<AvailabilityEntity> findAllByArticleIdIn(List<String> articleId, Pageable pageable);

  Page<AvailabilityEntity> findAll(Pageable pageable);

  AvailabilityEntity save(AvailabilityEntity availabilityEntity);

  Long deleteAvailabilityEntityById(String id);

  void deleteById(String id);
}