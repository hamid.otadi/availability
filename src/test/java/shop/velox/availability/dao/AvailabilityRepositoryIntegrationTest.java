package shop.velox.availability.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import shop.velox.availability.AvailabilityApplicationTests;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.availability.model.AvailabilityStatus;

@ExtendWith(SpringExtension.class)
class AvailabilityRepositoryIntegrationTest extends AvailabilityApplicationTests {

  private static final Logger LOG = LoggerFactory
      .getLogger(AvailabilityRepositoryIntegrationTest.class);

  @Resource
  private AvailabilityRepository availabilityRepository;

  @BeforeEach
  public void cleanDB() {
    availabilityRepository.deleteAll();
    availabilityRepository.flush();
    assertEquals(0L, availabilityRepository.findAll(Pageable.unpaged()).getTotalElements());
  }

  @Test
  @Order(1)
  public void auditTest() {
    AvailabilityEntity savedEntity = availabilityRepository
        .save(new AvailabilityEntity("articleId", BigDecimal.valueOf(123.45),
            AvailabilityStatus.IN_STOCK));

    assertNotNull(savedEntity.getCreateTime());
    assertNotNull(savedEntity.getModifiedTime());
  }

  @Test
  @Order(2)
  public void TotalNumberOfAvailabilitiesTest() {

    String articleId = "articleId";
    double priceValue = 123.45;
    availabilityRepository.save(new AvailabilityEntity(articleId, BigDecimal.valueOf(priceValue),
        AvailabilityStatus.IN_STOCK));
    assertEquals(1L, availabilityRepository.findAll(Pageable.unpaged()).getTotalElements());
  }

  @Test
  @Order(3)
  public void findAllByFiltersTest() {
    // If
    var articleId = "articleId";
    availabilityRepository.save(new AvailabilityEntity(articleId, BigDecimal.valueOf(0L),
        AvailabilityStatus.NOT_AVAILABLE));

    availabilityRepository.save(
        new AvailabilityEntity(articleId, BigDecimal.valueOf(3L), AvailabilityStatus.IN_STOCK));

    availabilityRepository.save(new AvailabilityEntity("otherArticleId", BigDecimal.valueOf(123.45),
        AvailabilityStatus.IN_STOCK));

    // When
    Page<AvailabilityEntity> results = availabilityRepository
        .findAllByArticleIdIn(List.of(articleId, "unknownArticleId"), Pageable.unpaged());

    // Then
    assertEquals(2, results.getContent().size());
    assertEquals(articleId, results.getContent().get(0).getArticleId());
    assertEquals(articleId, results.getContent().get(1).getArticleId());
  }

  @Test
  @Order(4)
  public void findByIdTest() {

    String articleId = "articleId";
    double priceValue = 123.45;
    AvailabilityEntity existing = availabilityRepository.save(
        new AvailabilityEntity(articleId, BigDecimal.valueOf(priceValue),
            AvailabilityStatus.IN_STOCK));
    assertEquals(1L, availabilityRepository.findAll(Pageable.unpaged()).getTotalElements());

    assertThat(availabilityRepository.findOneById(existing.getId())).isNotEmpty();
    assertThat(availabilityRepository.findOneByArticleId(existing.getArticleId())).isNotEmpty();
  }
}
