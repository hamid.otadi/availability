package shop.velox.availability;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles({"embeddeddb", "localauth"})
@TestMethodOrder(OrderAnnotation.class)
public class AvailabilityApplicationTests {

  @Test
  void contextLoads() {
  }

}